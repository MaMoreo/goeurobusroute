# README #

### What is this repository for? ###

* A web service that finds a connection between two bus stations.
* Code Challenge for GoEuro!
* 0.0.1

### How do I get set up? ###
No setup is needed for this app.


### HOW TO RUN THE APP ###

Use the service.sh to build and run the app:
 >./service.sh dev_bulld
 
 >./service.sh dev_run path/to/file

Or do it manually:

Perform two steps to start the application: 

1) Build the application from the command line with Maven. 
  >./mvn clean package

2)  Run the JAR file:
  >java -jar target\bus.route.challenge-0.0.1.jar --filePath=path\to\file

Open a browser and go to http://localhost:8088/api/direct?dep_sid={}&arr_sid={} to perform a query
Replace {} for the stations id's.

The answer (true or false) should appear shortly.

### DESIGN ###
* App 		: To make the application runnable.
* Controller	: RouteController a REST controller.
* Exceptions	: Object to handle the exceptions.
* Model	 	: RouteQuery (Object to perform the query)
			      RouteResult (Response of the Controller) 
* Resources	: HTML, js Files to show the error page.
* The test folder with: Unit Tests
						Resources

NOTES:
	The application expects an argument --filePath with the path to a valid
	bus route file.
	
	>java -jar target/bus.route.challenge-0.0.1.jar --filePath=\a\valid\path
		

Test Provided:
	Unit Tests


### Dependencies ###
* Spring Boot starters
* Thymeleaf 
* Apache Commons IO
* Doctusoft - Json Schema validation.


### Database configuration ###
* No Database is used for this application.

### How to run tests ###
* There is a test suite to run the unit tests.

* Smoke test can also be run with
 >./service.sh dev_run path/to/file
	
 >./service.sh dev_smoke
 
### CI ###
CI with Pipelines

### ENHANCEMENTS ###

- Provide more tests:
	Integration Tests	
	Sanity Tests
- Use Log classes during the application


### Who do I talk to? ###

* miguelangel.moreo@gmail.com


