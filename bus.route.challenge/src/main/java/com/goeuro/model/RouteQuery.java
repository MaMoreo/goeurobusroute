package com.goeuro.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import org.springframework.http.HttpStatus;

import com.goeuro.exception.RouteQueryException;

/**
 * Check if a direct route exists between two given stations.
 * 
 * @author Miguel Moreo
 */
public class RouteQuery {
	private String routesFilePath;
	private int departure;
	private int arrival;

	/**
	 * Constructor.
	 * 
	 * @param routesFilePath
	 *            The path to the file containing the bus routes.
	 */
	public RouteQuery(String routesFilePath) {
		this.routesFilePath = routesFilePath;
	}

	/**
	 * Finds a direct route connecting departure and arrival in a route File.
	 * 
	 * @param departure
	 *            Id of the departure station.
	 * @param arrival
	 *            Id of the destination station.
	 * @return {@code true} if there is a direct route between departure and
	 *         destination, {@code false} otherwise
	 */
	public boolean search(int departure, int arrival) throws RouteQueryException {
		// FIXME: we pass here these arguments because they are needed in a
		// deeper function
		// findStationsInRoute. How can we do this better?
		this.departure = departure;
		this.arrival = arrival;
		boolean routeExits = false;

		// A station is always connected to itself
		if (departure == arrival) {
			return true;
		}
		
		try (BufferedReader inputFile = new BufferedReader(new FileReader(routesFilePath))) {
			inputFile.readLine(); // skip the first line, we're not interested on it
			routeExits = processFile(inputFile);
		} catch (IOException e) {
			throw new RouteQueryException("Error handling file: " + e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return routeExits;
	}

	/**
	 * Reads line by line searching for the stations.
	 * 
	 * @param inputFile
	 *            A file containing the bus routes
	 * @return {@code true} if both stations are found in a route, <br>
	 *         {@code false} otherwise
	 * @throws IOException
	 */
	private boolean processFile(BufferedReader inputFile) throws IOException {
		String line;
		boolean routeExist = false;
		while ((line = inputFile.readLine()) != null && !routeExist) {
			line = line.substring(2);
			routeExist = Stream.of(line) //
					.map(findStationsInRoute) //
					.filter(i -> i == true) //
					.findFirst().isPresent();
		}
		return routeExist;
	}

	/**
	 * Finds a direct route connecting departure and arrival in a route File.
	 * 
	 * @param departure
	 *            Id of the departure station.
	 * @param arrival
	 *            Id of the destination station.
	 * @return {@code true} if there is a direct route between departure and
	 *         destination, {@code false} otherwise
	 * @deprecated This method reads the WHOLE file, that is a mistake if the
	 *             file is very large. Use {@link RouteQuery.query} instead
	 */
	public boolean search2(int departure, int arrival) throws RouteQueryException {
		this.departure = departure;
		this.arrival = arrival;
		boolean routeExits = false;

		// A station is always connected to itself
		if (departure == arrival) {
			return true;
		}

		try {
			Optional<Boolean> findFirst = Files.lines(Paths.get(routesFilePath)) //
					.skip(1) // skip the first element: number of lines in file
					.map(l -> l.substring(2)) // skip the route identifier
					.map(findStationsInRoute) //
					.filter(i -> i == true) //
					.findFirst();

			routeExits = findFirst.isPresent();
		} catch (IOException e) {
			throw new RouteQueryException("Error handling file: " + e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return routeExits;
	}

	/**
	 * Finds a direct route connecting departure and arrival in a route.
	 * 
	 * @return {@code true} if there is a direct route between departure and
	 *         destination, {@code false} otherwise
	 */
	private Function<String, Boolean> findStationsInRoute = (line) -> {
		int[] result = Stream.of(line.split(" ")) //
				.mapToInt(Integer::parseInt) //
				.filter(i -> i == departure || i == arrival) //
				.toArray();

		// Both stations were found, in any order.
		boolean routeExists = false;
		if (result.length == 2) {
			routeExists = (result[0] == departure && result[1] == arrival)
					|| (result[1] == departure && result[0] == arrival);
		}
		return routeExists;
	};

	// Getters and Setters
	public String getRoutesFilePath() {
		return routesFilePath;
	}

	public void setRoutesFilePath(String routesFilePath) {
		this.routesFilePath = routesFilePath;
	}
}
