package com.goeuro.model;

/**
 * This class represents a Query for a route. It consists on the departure and
 * destination stations, represented by their ids.
 * 
 * @author Miguel Moreo
 *
 */
public class RouteResult {
	private int dep_sid;
	private int arr_sid;
	private boolean direct_bus_route;

	public RouteResult(int depStationId, int arrStationId, boolean directBusRoute) {
		super();
		this.dep_sid = depStationId;
		this.arr_sid = arrStationId;
		this.direct_bus_route = directBusRoute;
	}

	public int getDep_sid() {
		return dep_sid;
	}

	public void setDep_sid(int dep_sid) {
		this.dep_sid = dep_sid;
	}

	public int getArr_sid() {
		return arr_sid;
	}

	public void setArr_sid(int arr_sid) {
		this.arr_sid = arr_sid;
	}

	public boolean isDirect_bus_route() {
		return direct_bus_route;
	}

	public void setDirect_bus_route(boolean direct_bus_route) {
		this.direct_bus_route = direct_bus_route;
	}
}
