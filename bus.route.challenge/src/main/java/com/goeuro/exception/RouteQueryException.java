package com.goeuro.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
@SuppressWarnings("serial")
public class RouteQueryException extends RuntimeException {

	private HttpStatus statusCode = null;
	
	/**
	 * There was a problem parsing the URL.
	 */
	public RouteQueryException(String message, HttpStatus status) {
		super("Problem performing the query due to '" + message + "'.");
		statusCode = status;
	}
	
	/**
	 * There was a problem parsing the URL.
	 */
	public RouteQueryException(String message) {
		super("Problem performing the query due to '" + message + "'.");
	}

	public HttpStatus getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}
}
