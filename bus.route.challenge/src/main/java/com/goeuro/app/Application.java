package com.goeuro.app;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * MicroService that answers if there is a direct route from one bus station to
 * another.
 * 
 * Usage: http://localhost:8088/api/direct?dep_sid={}&arr_sid={}
 * 
 * To start the application provide the path to the bus route file as the first
 * command line argument example: --filePath=[path/to/file]
 * 
 * @author Miguel Moreo
 */
@SpringBootApplication(scanBasePackages = { "com.goeuro.controller" })
public class Application implements ApplicationRunner {

	private static final Logger logger = LoggerFactory.getLogger(Application.class);

	/**
	 * Application Entry point.
	 * 
	 * @param arg0
	 *            The command line argument contains a path to a bus route file.
	 */
	public static void main(String[] arg0) {
		SpringApplication.run(Application.class, arg0);
	}

	@Override
	public void run(ApplicationArguments arg0) throws Exception {
		try {
			argumentsValidation(arg0);
			fileValidation(arg0);
		} catch (IllegalArgumentException e) {
			logger.error(e.getMessage());
			System.exit(-1);
		}
	}

	/**
	 * Checks that the arguments provided contains the expected --filePath
	 * 
	 * @param args
	 *            Application arguments
	 * @throws IllegalArgumentException
	 */
	public void argumentsValidation(ApplicationArguments args) throws IllegalArgumentException {
		if (args.getSourceArgs() == null || args.getSourceArgs().length <= 0 || !Stream.of(args.getSourceArgs()) //
				.filter(l -> l.startsWith("--filePath=")) //
				.findFirst() //
				.isPresent()) { //
			throw new IllegalArgumentException("ERROR: No file path found. Provide it as: --filePath=[path/to/file]");
		}
	}
	
	/**
	 * Checks that the argument provided points to a valid File.
	 * 
	 * @param args
	 *            Application arguments
	 * @throws IllegalArgumentException
	 */
	public void fileValidation(ApplicationArguments args) throws IllegalArgumentException {
		for (String name : args.getOptionNames()) {
			if ("filePath".equals(name)) {
				if (!Files.exists(Paths.get(args.getOptionValues(name).get(0)))) {
					throw new IllegalArgumentException(
							"ERROR: The path provided is not valid: " + args.getOptionValues(name).get(0));
				}
			}
		}
	}
}
