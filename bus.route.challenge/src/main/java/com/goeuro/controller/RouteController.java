package com.goeuro.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.goeuro.exception.RouteQueryException;
import com.goeuro.model.RouteQuery;
import com.goeuro.model.RouteResult;

/**
 * Rest Controller.
 *
 * @author Miguel Moreo
 */
@RestController
public class RouteController {

	@Value("${filePath}")
	private String filePath; // the bus routes file

	@GetMapping(value = "/api/direct")
	public ResponseEntity<RouteResult> getRoute(@RequestParam("dep_sid") int depStationId,
			@RequestParam("arr_sid") int arrStationId) {

		HttpStatus status = HttpStatus.ACCEPTED;
		boolean routeExits = false;
		try {
			checkValidQueryParameters(depStationId, arrStationId);
			RouteQuery query = new RouteQuery(filePath);
			routeExits = query.search(depStationId, arrStationId);
		} catch (RouteQueryException e) {
			status = e.getStatusCode();
		}
		return new ResponseEntity<RouteResult>(new RouteResult(depStationId, arrStationId, routeExits), status);
	}

	/**
	 * Checks the parameters to perform the query are valid
	 * 
	 * @param depStationId
	 *            Id of the departure station.
	 * @param arrStationId
	 *            Id of the destination station.
	 */
	private void checkValidQueryParameters(int depStationId, int arrStationId) {
		if (depStationId < 0 || depStationId > 1000000 || arrStationId < 0 || arrStationId > 1000000) {
			throw new RouteQueryException("The provided parameters are out of the scope", HttpStatus.BAD_REQUEST);
		}
	}
}
