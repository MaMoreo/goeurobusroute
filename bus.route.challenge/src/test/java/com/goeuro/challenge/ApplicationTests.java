package com.goeuro.challenge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.springframework.boot.ApplicationArguments;

import com.goeuro.app.Application;

public class ApplicationTests {

	private static final String ERROR_PATH_NOT_VALID = "ERROR: The path provided is not valid: src/test/resources/bogus_file";
	private static final String ERROR_NO_FILE_PATH = "ERROR: No file path found. Provide it as: --filePath=[path/to/file]";

	/**
	 * Tests that the application expects --filePath argument.
	 */
	@Test
	public void argumentValidTest() throws Exception {
		Application application = new Application();
		application.argumentsValidation(new ApplicationArguments() {

			@Override
			public String[] getSourceArgs() {
				String[] args = { "--filePath=/a/file/path" };
				return args;
			}

			@Override
			public List<String> getOptionValues(String arg0) {
				return null;
			}

			@Override
			public Set<String> getOptionNames() {
				return null;
			}

			@Override
			public List<String> getNonOptionArgs() {
				return null;
			}

			@Override
			public boolean containsOption(String arg0) {
				return false;
			}
		});
		assertTrue(true); // the method finished without problems
	}

	/**
	 * Tests arguments provided are empty
	 * 
	 * @throws IllegalArgumentException
	 */
	@Test(expected = IllegalArgumentException.class)
	public void argumentValidEmptyTest() throws Exception {
		Application application = new Application();
		try {
			application.argumentsValidation(new ApplicationArguments() {

				@Override
				public String[] getSourceArgs() {
					return null;
				}

				@Override
				public List<String> getOptionValues(String arg0) {
					return null;
				}

				@Override
				public Set<String> getOptionNames() {
					return null;
				}

				@Override
				public List<String> getNonOptionArgs() {
					return null;
				}

				@Override
				public boolean containsOption(String arg0) {
					return false;
				}
			});
		} catch (IllegalArgumentException e) {
			assertEquals(ERROR_NO_FILE_PATH, e.getMessage());
			throw e;
		}
	}

	/**
	 * Tests the path --filePath is valid.
	 */
	@Test
	public void fileValidationTest() throws Exception {
		Application application = new Application();

		application.fileValidation(new ApplicationArguments() {

			@Override
			public String[] getSourceArgs() {
				String[] args = { "--filePath=src/test/resources/bus_route_data_file" };
				return args;
			}

			@Override
			public List<String> getOptionValues(String arg0) {
				List<String> values = new ArrayList<String>();
				values.add("src/test/resources/bus_route_data_file");
				return values; // always return the expected value
			}

			@Override
			public Set<String> getOptionNames() {
				Set<String> names = new HashSet<String>();
				names.add("filePath");
				return names;
			}

			@Override
			public List<String> getNonOptionArgs() {
				return null;
			}

			@Override
			public boolean containsOption(String arg0) {
				return false;
			}
		});
		assertTrue(true); // the method finished without problems
	}

	/**
	 * Tests the path --filePath is not valid.
	 * 
	 * @throws IllegalArgumentException
	 */
	@Test(expected = IllegalArgumentException.class)
	public void fileValidationWrongTest() throws Exception {
		Application application = new Application();
		try {
			application.fileValidation(new ApplicationArguments() {

				@Override
				public String[] getSourceArgs() {
					String[] args = { "--filePath=src/test/resources/bogus_file" };
					return args;
				}

				@Override
				public List<String> getOptionValues(String arg0) {
					// always return the expected value
					List<String> values = new ArrayList<String>();
					values.add("src/test/resources/bogus_file");
					return values;
				}

				@Override
				public Set<String> getOptionNames() {
					Set<String> names = new HashSet<String>();
					names.add("filePath");
					return names;
				}

				@Override
				public List<String> getNonOptionArgs() {
					return null;
				}

				@Override
				public boolean containsOption(String arg0) {
					return false;
				}
			});
		} catch (IllegalArgumentException e) {
			assertEquals(ERROR_PATH_NOT_VALID, e.getMessage());
			throw e;
		}
	}
}
