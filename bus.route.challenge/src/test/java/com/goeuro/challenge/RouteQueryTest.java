/**
 * 
 */
package com.goeuro.challenge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.http.HttpStatus;

import com.goeuro.exception.RouteQueryException;
import com.goeuro.model.RouteQuery;

/**
 * Test Case for {@link RouteQuery} class
 * @author Miguel Moreo
 */
public class RouteQueryTest {

	/**
	 * Test method for {@link com.goeuro.model.RouteQuery#search()}.
	 * <br>Tests several queries.
	 */
	@Test
	public void testSearch() {
		String filePath = "src/test/resources/bus_route_data_file";
		RouteQuery query = new RouteQuery(filePath);

		// check some stations
		assertTrue(query.search(1, 2));
		assertFalse(query.search(23, 2));

		// check both ways
		assertTrue(query.search(5, 3));
		assertTrue(query.search(3, 5));

		// check the same station
		assertTrue(query.search(5, 5));
	}

	/**
	 * Test method for {@link com.goeuro.model.RouteQuery#search()}.
	 * <br>Tests Exception when reading an invalid file.
	 */
	@Test(expected = RouteQueryException.class)
	public void testSearchException() {
		// A file that does not exist
		String filePath = "src/test/resources/bogus_file";
		RouteQuery query = new RouteQuery(filePath);

		try {
			query.search(5, 6);
		} catch (RouteQueryException e) {
			assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
			throw e;
		}
	}
}
